package com.elliott.nycschools.schoolinfo

import com.elliott.nycschools.accessory.MainDispatcherRule
import com.elliott.nycschools.network.model.HighSchoolResponseItem
import com.elliott.nycschools.network.repository.HighSchoolRepository
import com.elliott.nycschools.network.retrofit.CallState
import com.elliott.nycschools.network.retrofit.NetworkResult
import com.elliott.nycschools.ui.viewmodel.SchoolListViewModel
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.impl.annotations.MockK
import junit.framework.TestCase.assertTrue
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class SchoolListViewModelTest {

    @get:Rule
    val mainDispatcherRule = MainDispatcherRule()

    @MockK
    lateinit var mockSchoolRepository: HighSchoolRepository

    lateinit var viewModel: SchoolListViewModel

    private val item1 = HighSchoolResponseItem(
        dbn = "dbn1",
        schoolName = "Dull Rock University"
    )
    private val item2 = HighSchoolResponseItem(
        dbn = "dbn2",
        schoolName = "Genius University"
    )
    private val items = listOf(
        item1,
        item2
    )

    @Before
    fun setUp() {
        MockKAnnotations.init(this)
        viewModel = SchoolListViewModel(mockSchoolRepository)
    }

    @Test
    fun `getSchools should update highSchoolNameList with correct data`() = runTest {
        val mockResponse =
            NetworkResult.Success(items)
        coEvery { mockSchoolRepository.getHighSchoolNames() } returns flowOf(mockResponse)
        viewModel.getSchools()
        assert(viewModel.highSchoolNameList.value is CallState.Successful)

        assertTrue(viewModel.highSchoolNameList.value is CallState.Successful)
    }

    @Test
    fun `getSchools should update highSchoolNameList with success state`() = runTest {
        val mockResponse =
            NetworkResult.Success(items)
        coEvery { mockSchoolRepository.getHighSchoolNames() } returns flowOf(mockResponse)
        viewModel.getSchools()
        assert(viewModel.highSchoolNameList.value is CallState.Successful)

        assertTrue(mockResponse.meta?.get(0)?.schoolName?.equals((viewModel.highSchoolNameList.value as CallState.Successful).data?.get(0)) == true)
    }

    @Test
    fun `getSchools should update highSchoolNameList with empty state if response is empty`() = runTest {
        val mockResponse = NetworkResult.Success(listOf<HighSchoolResponseItem>())
        coEvery { mockSchoolRepository.getHighSchoolNames() } returns flowOf(mockResponse)
        viewModel.getSchools()
        assert(viewModel.highSchoolNameList.value is CallState.Empty)

        assertTrue(viewModel.highSchoolNameList.value is CallState.Empty)
    }

    @Test
    fun `getSchools should update highSchoolNameList with error if network call fails`() = runTest {
        val mockResponse = NetworkResult.Error<List<HighSchoolResponseItem>>(data = "", responseCode = 400)
        coEvery { mockSchoolRepository.getHighSchoolNames() } returns flowOf(mockResponse)
        viewModel.getSchools()
        assert(viewModel.highSchoolNameList.value is CallState.Error)
    }
}
package com.elliott.nycschools.schoolinfo

import com.elliott.nycschools.accessory.MainDispatcherRule
import com.elliott.nycschools.network.model.HighSchoolResponseItem
import com.elliott.nycschools.network.repository.HighSchoolRepository
import com.elliott.nycschools.network.retrofit.CallState
import com.elliott.nycschools.network.retrofit.NetworkResult
import com.elliott.nycschools.ui.model.HighSchool
import com.elliott.nycschools.ui.viewmodel.SchoolInfoViewModel
import com.nhaarman.mockitokotlin2.whenever
import junit.framework.TestCase.assertTrue
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mockito.mock

class SchoolInfoViewModelTest {

    private lateinit var viewModel: SchoolInfoViewModel
    private lateinit var repository: HighSchoolRepository
    private val dbn = "dbn"
    private val schoolName = "Test School"
    private val satCriticalReadingAverageScore = "Test School"
    private val satMathAvgScore = "550"
    private val satWritingAvgScore = "450"
    private val numOfSatTestTakers = "100"

    @get:Rule
    val mainDispatcherRule = MainDispatcherRule()

    @Before
    fun setup() {
        repository = mock(HighSchoolRepository::class.java)
        viewModel = SchoolInfoViewModel(repository)
    }

    @Test
    fun `getSchoolInfo with valid school name returns successful result`() = runTest {
        val highSchoolResponse = HighSchoolResponseItem(
            dbn = dbn,
            schoolName = schoolName,
            satCriticalReadingAvgScore = satCriticalReadingAverageScore,
            satMathAvgScore = satMathAvgScore,
            satWritingAvgScore = satWritingAvgScore,
            numOfSatTestTakers = numOfSatTestTakers
        )
        whenever(repository.getHighSchoolDataByName(schoolName)).thenReturn(
            flowOf(NetworkResult.Success(listOf(highSchoolResponse)))
        )
        viewModel.getSchoolInfo(schoolName)
        assertTrue(viewModel.highSchool.first() is CallState.Successful)
    }

    @Test
    fun `getSchoolInfo with valid school name returns result with sanity checked fields`() = runTest {
        val highSchoolResponse = HighSchoolResponseItem(
            dbn = dbn,
            schoolName = schoolName,
            satCriticalReadingAvgScore = satCriticalReadingAverageScore,
            satMathAvgScore = satMathAvgScore,
            satWritingAvgScore = satWritingAvgScore,
            numOfSatTestTakers = numOfSatTestTakers
        )
        val expected = HighSchoolResponseItem(
            dbn = dbn,
            schoolName = schoolName,
            satCriticalReadingAvgScore = satCriticalReadingAverageScore,
            satMathAvgScore = satMathAvgScore,
            satWritingAvgScore = satWritingAvgScore,
            numOfSatTestTakers = numOfSatTestTakers
        )
        whenever(repository.getHighSchoolDataByName(schoolName)).thenReturn(
            flowOf(NetworkResult.Success(listOf(highSchoolResponse)))
        )
        viewModel.getSchoolInfo(schoolName)

        assertTrue(highSchoolObjectSanityCheck(expected, (viewModel.highSchool.first() as CallState.Successful).data!!))
    }

    @Test
    fun `getSchoolInfo with empty response returns empty result`() = runTest {
        whenever(repository.getHighSchoolDataByName(schoolName)).thenReturn(
            flowOf(NetworkResult.Success(emptyList()))
        )
        viewModel.getSchoolInfo(schoolName)
        assertTrue(viewModel.highSchool.first() is CallState.Empty)
    }

    @Test
    fun `getSchoolInfo with error response reflects in error state`() = runTest {
        whenever(repository.getHighSchoolDataByName(schoolName)).thenReturn(
            flowOf(NetworkResult.Error(Exception(), responseCode = 400))
        )
        viewModel.getSchoolInfo(schoolName)
        assertTrue(viewModel.highSchool.first() is CallState.Error)
    }

    private fun highSchoolObjectSanityCheck(
        highSchoolResponseItem: HighSchoolResponseItem,
        highSchool: HighSchool
    ): Boolean {
        return highSchoolResponseItem.numOfSatTestTakers?.equals(highSchool.numOfSatTestTakers) == true &&
                highSchoolResponseItem.schoolName?.equals(highSchool.name) == true &&
                highSchoolResponseItem.satMathAvgScore?.equals(highSchool.satMathAvgScore) == true
    }
}
package com.elliott.nycschools.constant

object NavRoutes {
    const val SCHOOL_INFO_SCREEN = "SCHOOL_INFO_SCREEN"
    const val MAIN_SCREEN = "MAIN_SCREEN"
}
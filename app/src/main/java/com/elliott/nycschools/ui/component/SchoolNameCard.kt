package com.elliott.nycschools.ui.component

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.requiredHeightIn
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import com.elliott.nycschools.constant.NavRoutes
import com.elliott.nycschools.ui.theme.SchoolAnimationDimen
import com.elliott.nycschools.util.encodeUTF8

@Composable
fun SchoolNameCard(
    navController: NavController,
    highSchoolList: List<String>,
    it: Int
) {
    Card(
        modifier = Modifier
            .fillMaxWidth()
            .requiredHeightIn(
                min = 64.dp,
                max = SchoolAnimationDimen
            )
            .padding(15.dp)
            .clickable {
                navController.navigate("${NavRoutes.SCHOOL_INFO_SCREEN}/${highSchoolList[it].encodeUTF8()}")
            },
        colors = CardDefaults.cardColors(
            containerColor = MaterialTheme.colorScheme.surfaceVariant,
        ),
        elevation = CardDefaults.cardElevation(
            defaultElevation = 4.dp
        )
    ) {
        Box(
            Modifier.fillMaxSize(),
            contentAlignment = Alignment.Center
        ) {
            Text(
                text = highSchoolList[it],
                textAlign = TextAlign.Center,
                maxLines = 2,
                overflow = TextOverflow.Ellipsis
            )
        }
    }
}
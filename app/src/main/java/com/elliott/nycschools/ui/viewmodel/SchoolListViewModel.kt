package com.elliott.nycschools.ui.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.elliott.nycschools.network.repository.HighSchoolRepository
import com.elliott.nycschools.network.retrofit.CallState
import com.elliott.nycschools.network.retrofit.NetworkResult
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import org.koin.core.component.KoinComponent

class SchoolListViewModel(private val schoolRepository: HighSchoolRepository) : ViewModel(),
    KoinComponent {
    // TODO: add tests for business logic

    private val _highSchoolNameList =
        MutableStateFlow<CallState<List<String>>>(CallState.NotExecutedYet())
    val highSchoolNameList: StateFlow<CallState<List<String>>> = _highSchoolNameList

    init {
        getSchools()
    }

    // TODO : Alphabetize the schools before displaying them to help a user find their school
    fun getSchools() = viewModelScope.launch {
        _highSchoolNameList.value = CallState.Loading()
        schoolRepository.getHighSchoolNames().collect {
            _highSchoolNameList.value = if (it is NetworkResult.Success) {
                when (it.meta?.isEmpty()) {
                    true -> CallState.Empty()
                    else -> {
                        val highSchoolList = mutableListOf<String>()
                        it.meta?.forEach { response ->
                            response.schoolName?.let { name -> highSchoolList.add(name) }
                        }

                        val sortedList = highSchoolList.sorted()
                        CallState.Successful(sortedList)
                    }
                }
            } else {
                CallState.Error()
            }
        }
    }
}
package com.elliott.nycschools.ui.screen

import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.grid.GridCells
import androidx.compose.foundation.lazy.grid.LazyVerticalGrid
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.res.stringResource
import androidx.navigation.NavController
import com.elliott.nycschools.R
import com.elliott.nycschools.network.retrofit.CallStateContent
import com.elliott.nycschools.ui.component.SchoolNameCard
import com.elliott.nycschools.ui.component.ShimmerAnimateGridItem
import com.elliott.nycschools.ui.viewmodel.SchoolListViewModel
import org.koin.androidx.compose.getViewModel

@Composable
fun SchoolListScreen(navController: NavController) {
    val schoolInfoViewModel = getViewModel<SchoolListViewModel>()
    val schoolListState = rememberLazyListState()

    val highSchoolList by schoolInfoViewModel.highSchoolNameList.collectAsState()
    CallStateContent(callState = highSchoolList,
        loadingUI = {
            LazyVerticalGrid(GridCells.Fixed(1)) {
                repeat(15) {
                    item { ShimmerAnimateGridItem() }
                }
            }
        },
        errorUI = {
            // TODO: Would have a network error screen, as well as a try again button
        }, emptyUI = {
            Text(stringResource(R.string.text_empty_error))
        }
    ) { highSchoolList ->
        // TODO: paginate this
        LazyColumn(
            content = {
                highSchoolList?.size?.let { size ->
                    this.items(
                        count = size,
                        itemContent = {
                            SchoolNameCard(navController, highSchoolList, it)
                        }
                    )
                }
            },
            state = schoolListState
        )

    }
}
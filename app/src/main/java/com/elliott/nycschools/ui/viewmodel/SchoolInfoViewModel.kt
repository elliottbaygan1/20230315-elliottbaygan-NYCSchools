package com.elliott.nycschools.ui.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.elliott.nycschools.network.repository.HighSchoolRepository
import com.elliott.nycschools.network.retrofit.CallState
import com.elliott.nycschools.network.retrofit.NetworkResult
import com.elliott.nycschools.ui.model.HighSchool
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import org.koin.core.component.KoinComponent

class SchoolInfoViewModel(private val schoolRepository: HighSchoolRepository) : ViewModel(), KoinComponent {

    private val _highSchool: MutableStateFlow<CallState<HighSchool>> =
        MutableStateFlow(CallState.NotExecutedYet())
    val highSchool: StateFlow<CallState<HighSchool>> = _highSchool

    fun getSchoolInfo(schoolName: String) = viewModelScope.launch {
        _highSchool.value = CallState.Loading()
        schoolRepository.getHighSchoolDataByName(schoolName)
            .collect {
                if (it is NetworkResult.Success) {
                    if (it.meta?.isEmpty() == true) {
                        _highSchool.value = CallState.Empty()
                    } else {
                        val response = it.meta?.get(0)
                        val school = HighSchool(
                            name = response?.schoolName,
                            satCriticalReadingAvgScore = response?.satCriticalReadingAvgScore,
                            satMathAvgScore = response?.satMathAvgScore,
                            satWritingAvgScore = response?.satWritingAvgScore,
                            numOfSatTestTakers = response?.numOfSatTestTakers
                        )
                        _highSchool.value = CallState.Successful(school)
                    }
                } else {
                    _highSchool.value = CallState.Error()
                }
            }
    }

}
package com.elliott.nycschools.di

import com.elliott.nycschools.network.repository.HighSchoolRepository
import org.koin.dsl.module

val repositoryModule = module {
    single { HighSchoolRepository(get()) }
}
package com.elliott.nycschools.di

import com.elliott.nycschools.ui.viewmodel.SchoolInfoViewModel
import com.elliott.nycschools.ui.viewmodel.SchoolListViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewModelModule = module {
    viewModel { SchoolListViewModel(get()) }
    viewModel { SchoolInfoViewModel(get()) }
}
package com.elliott.nycschools.network.retrofit

import androidx.compose.runtime.Composable

/**
 * Call state Data for network dependent composables
 */
sealed class CallState<T> {
    class Successful<T>(val data: T?) : CallState<T>()
    class Loading<T> : CallState<T>()
    class Error<T> : CallState<T>()
    class Empty<T> : CallState<T>()
    class NotExecutedYet<T> : CallState<T>()
}

/**
 * For use displaying content that depends on the state of an API call
 */
@Composable
fun <T> CallStateContent(
    callState: CallState<T>,
    errorUI: @Composable () -> Unit,
    loadingUI: @Composable () -> Unit,
    emptyUI: @Composable () -> Unit,
    successUI: @Composable (T?) -> Unit,
) {
    when (callState) {
        is CallState.Successful -> successUI(callState.data)
        is CallState.Empty -> emptyUI()
        is CallState.Error -> errorUI()
        is CallState.Loading -> loadingUI()
        else -> {}
    }
}